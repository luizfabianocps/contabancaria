package br.com.itau;

public class Conta {

    private double saldo;
    private String numeroConta;
    private Cliente cliente;
    private int numeroAgencia;


    public Conta(double saldo, String numeroConta, Cliente cliente, int numeroAgencia) {
        this.saldo = saldo;
        this.numeroConta = numeroConta;
        this.cliente = cliente;
        this.numeroAgencia = numeroAgencia;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public String getNumeroConta() {
        return numeroConta;
    }

    public void setNumeroConta(String numeroConta) {
        this.numeroConta = numeroConta;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public int getNumeroAgencia() {
        return numeroAgencia;
    }

    public void setNumeroAgencia(int numeroAgencia) {
        this.numeroAgencia = numeroAgencia;
    }

    public double sacar() {


        return 0;
    }

    public void depositarNaConta(double valorDeposito) {
        saldo = saldo + valorDeposito;
    }

    public void sacarDaConta (double valorSaque) {
        saldo = saldo - valorSaque;
    }

}
