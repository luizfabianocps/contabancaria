package br.com.itau;

public class Main {

    public static void main(String[] args) {

        try {
            Cliente cliente = new Cliente(
                    "Luiz",
                    "561.423.940-55",
                    21
            );

            Conta conta = new Conta(
                    10000.00,
                    "05201-2",
                    cliente,
                    341
            );

            conta.depositarNaConta(320000.0);
            System.out.println(conta.getSaldo());
            conta.sacarDaConta(250000.00);
            System.out.println(conta.getSaldo());
            conta.depositarNaConta(220000.000);

            System.out.println("****************** SALDO DO DIA*************************");

            System.out.println(conta.getSaldo());

        } catch (ArithmeticException e) {
            System.out.println(e.getMessage());

        }


    }


}
